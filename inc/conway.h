// Run command:
// clear; make; ./bin/binary --play --dims 80 60 --rules 32 3

// Debug command:
// clear; make; gdb ./bin/binary -q

#include <GLFW/glfw3.h>
#include <stdio.h> 
#include <math.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>
#include <assert.h>

#define GLFW_INCLUDE_GLU

// These could have been functions, but their low complexity and reliance on only boolean logic
// means that this is better, as it uses much less memory.
#define AVCHK(x) 	strcmp(argv[i],x)==0     // Does argument array contain string x?
#define KP(x) 		glfwGetKey(window,x)	 // Is key down?
#define I2C(x) 		(char)(x+'0')			 // Convert int to char
#define C2I(x) 		(int)(x-'0')			 // Convert char to int

#define STRLEN 256 // Mostly arbitrary

#define MAX_WINDOW_WIDTH  1920
#define MAX_WINDOW_HEIGHT 1080
#define VSYNC 			  0
#define USE_DIALOGS		  true

#define MAX_CELLS_WIDE  400
#define MAX_CELLS_TALL  300
#define SPACING 		1		// Spacing between adajcent cells
#define CELL_SIZE 		14 		// Size of each cell (they are always square)
#define DEAD 			0       // Means I can refer to them as dead rather than false (more intuitive)
#define ALIVE 			1       // Means I can refer to them as dead rather than false
#define LOOP_TIME_LOW   0.1
#define LOOP_TIME_HGH   1.9
#define COOLDOWN 		1.5		// Cooldown between registered keypresses when key held
#define COLORLEN		3
#define COLORNUM		2
#define RAINBOWSPEED	1

// only run prototypes (etc) once.
// need to do this since it is #included multiple times
#ifndef GUARD
#define GUARD

typedef short cell_t; // using typedef for this lets me change the variable type used to describe cells easily

typedef struct CA {
	int width;
	int height;
	cell_t** cells;
	float color_matrix[COLORNUM][COLORLEN];
} cellArray;

// Prototype functions below

// Grid manipuation functions:

void set2d(int xmax,int ymax, cell_t** a, cell_t v);
void place(int x, int y, int n, cellArray* a);
int isSpace(int x, int y, int r);

// File I/O functions
void askLoad(cellArray* ca_intern);
void loadFile(char* filename, cellArray* ca_intern);
void askSave(cellArray* ca_intern);
void saveFile(cellArray* states,char* filename);

// Misc utility functions:

cell_t** create2d (int rows, int columns);
void destroy2d(int rows, cell_t** a);
int getRand(int max);
int clickedCX(int x);
int clickedCY(int y, int wh);

// Render functions:

void rectangle(float x, float y,float w, float h, float color[3]);
void pause_sign(int wh);
void place_sign(int n, int ww, int wh);

// Gameplay and core functions:

void update(cellArray* ts, cellArray* s, char * sr, char * br);
void render(cellArray* s, bool p, int pm, int ww, int wh);
int check_ns(int x, int y,cellArray* a);
void set(int x, int y, cellArray* a, cell_t val);

#endif