#include <conway.h>

// Function which returns a pointer to a dynamically allocated 2d array
cell_t** create2d (int rows, int columns){
	cell_t** p = (cell_t**) malloc(rows * sizeof(cell_t*));
	if (p == NULL) {
		printf("Error allocating memory! \n");
		exit(EXIT_FAILURE);
	}
	for (int i = 0; i < rows ; i++){
		p[i] = (cell_t*)  calloc(columns * sizeof(cell_t),sizeof(cell_t));
		if (p[i] == NULL) {
			printf("Error allocating memory! \n");
			exit(EXIT_FAILURE);
		}
	}
	return p;
}

// Frees memory allocated to a 2d array
void destroy2d(int rows, cell_t** a){
	for (int i = 0; i < rows ; i++){
		free(a[i]);
	}
	free(a);
}


// Sets a 2d array of given dimensions to a given value
void set2d(int xmax,int ymax, cell_t** a, cell_t v){
	for (int i = 0; i < xmax ; i++){
		for (int j = 0; j < ymax; j++){
			a[i][j] = v;
		}
	}
}

// Function to set the state of a given cell a->cells[x][y]
// Args: x_index, y_index, cellstates_struct, value
// Performs check on whether the cell actually exists, since cursor can travel outside of window
void set(int x, int y, cellArray *a, cell_t value){
	if (x < a->width && y < a->height && x >= 0 && y >= 0) {
		a->cells[x][y] = value;
	} else {
		printf("Trying set non-existent cell (%i,%i) to state '%i'\n",x,y,value);
	}
}

// Function to check how many of a cell's neighbours are alive
// Args: x_index, y_index, states_array
int check_ns(int x, int y, cellArray* a){
	int ns = 0;
	int cx, cy; // check x, check y

	// loop around cell vvvvv
	for (int i = -1; i <= 1; i++){
		for(int j = -1; j <= 1; j++){
			if(!(i == 0 && j == 0)){ // not centre cell
				cx = x + i;
				cy = y + j;
				if (cx >= 0 && cy >= 0 && cx < a->width && cy < a->height){
					if(a->cells[cx][cy] == ALIVE) ns += 1;
				}
			}
		}
	}
	return ns;
}

// Return cell corresponding the mouse click (x coordinate)
int clickedCX(int x){
	return x / (CELL_SIZE + SPACING);
}

// Return cell corresponding the mouse click (y coordinate)
int clickedCY(int y, int wh){
	return (wh - y) / (CELL_SIZE + SPACING);
}

// Get random number between 0 and _max
int getRand(int max){
	return rand() % max;
}

// Place a pattern based on which placemode is set
// To be called when the user left clicks
void place(int x, int y, int n, cellArray* a){
	switch(n){
		default:
			// When somehow an invalid - this shouldn't really happen
			printf("Invalid shape selected. Defaulting to no selection.\n");
			break;
		case 1:
			// Place a single cell at the cursor position
			set(x,y,a,ALIVE);
			break;
		case 2:
			// Place a 2x2 square
			set(x  ,y  ,a,ALIVE);
			set(x+1,y  ,a,ALIVE);
			set(x+1,y+1,a,ALIVE);
			set(x  ,y+1,a,ALIVE);
			break;
		case 3:
			// Place glider
			set(x+1,y  ,a,ALIVE);
			set(x+1,y+1,a,ALIVE);
			set(x+1,y-1,a,ALIVE);
			set(x  ,y-1,a,ALIVE);
			set(x-1,y  ,a,ALIVE);
			break;
		case 4:
			// Place small space ship
			set(x  ,y+2,a,ALIVE);
			set(x+1,y+2,a,ALIVE);
			set(x+2,y+2,a,ALIVE);
			set(x-1,y+2,a,ALIVE);
			set(x-2,y+1,a,ALIVE);
			set(x+2,y+1,a,ALIVE);
			set(x+2,y  ,a,ALIVE);
			set(x+1,y-1,a,ALIVE);
			set(x-2,y-1,a,ALIVE);
			break;
		case 5:
			// Place queen bee
			set(x  ,y+2,a,ALIVE);
			set(x  ,y-2,a,ALIVE);
			set(x-1,y  ,a,ALIVE);
			set(x-1,y+1,a,ALIVE);
			set(x-1,y-1,a,ALIVE);
			set(x+1,y+1,a,ALIVE);
			set(x+1,y-1,a,ALIVE);
			set(x+2,y  ,a,ALIVE);
			set(x-2,y+2,a,ALIVE);
			set(x-2,y+3,a,ALIVE);
			set(x-2,y-2,a,ALIVE);
			set(x-2,y-3,a,ALIVE);
			break;
	}
}
