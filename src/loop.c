#include <conway.h>

// Function containing update code
// Args: temp_states, states, survivalrules, birthrules
void update(cellArray* ts, cellArray* s, char * sr, char * br){
	int count; // total number of neighbours each cell has.
	
	// copy states to temp_states
	for (int i = 0; i < s->width; i++){
		for (int j = 0; j < s->height; j++){
			ts->cells[i][j] = s->cells[i][j];
		}
	}

	// make changes to alive/dead. make modifications to temp_states while reading from states.
	for (int i = 0; i < s->width; i++){
		for (int j = 0; j < s->height; j++){

			// Count number of neighbours for this cell
			count = check_ns(i,j,s); // so that the function is only run once per cell
			// Code below applies the above rules
			if (s->cells[i][j] == ALIVE) {
				// printf("Cell at (%i,%i) is ALIVE with %i neighbours.\n",i,j,count);
				if (strchr(sr,count +'0') != NULL) {
					// Survives
					ts->cells[i][j] = ALIVE;
				} else {
					ts->cells[i][j] = DEAD;
				}
			} else if (s->cells[i][j] == DEAD) {
				if (strchr(br,count +'0') != NULL) {
					// Born
					ts->cells[i][j] = ALIVE;
				}
			} else {
				// This shouldn't really happen
				printf("State error! Cell (%i,%i) has value %i\n",i,j,s->cells[i][j]);
			}
		}
	}
	// Copy temp_states to states
	for (int i = 0; i < s->width; i++){
		for (int j = 0; j < s->height; j++){
			s->cells[i][j] = ts->cells[i][j];
		}
	}
}

// Function containing the render code.
// Args: temp_states, states, playing, placemode, windowwidth, windowheight
void render(cellArray* s, bool p, int pm, int ww, int wh){

	for (int i = 0; i < s->width; i++){
		for (int j = 0; j < s->height; j++){
			switch (s->cells[i][j]){
				case ALIVE:
					rectangle(SPACING+(SPACING+CELL_SIZE) * i, SPACING+(SPACING+CELL_SIZE) * j, CELL_SIZE, CELL_SIZE,s->color_matrix[ALIVE]);
					break;
				case DEAD:
					rectangle(SPACING+(SPACING+CELL_SIZE) * i, SPACING+(SPACING+CELL_SIZE) * j, CELL_SIZE, CELL_SIZE,s->color_matrix[DEAD]);
					break;
				default:
					printf("Error evaluating cell state at (i,j) = (%i,%i). Exiting.",i,j);
					exit(EXIT_FAILURE);
			}
		}
	}
	if (p == false) {
		pause_sign(wh);
	}
	place_sign(pm, ww, wh);
}