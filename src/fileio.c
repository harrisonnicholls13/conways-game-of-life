#include <conway.h>

// Function to generate a loadfile prompt, and then call the function which reads it
void askLoad(cellArray* ca_intern){
	char filename[STRLEN];

	// generate file open dialog using zenity
	FILE* fp;
	fp = popen("zenity --file-selection --title='Select a File'","r");
	fgets(filename, sizeof(filename), fp);
	pclose(fp);
	for (int i = 0; i < STRLEN; i++){
		if(filename[i] == '\n'){
			filename[i] = '\0';
			break;
		}
	}

	if (access(filename,F_OK) != 0){
		printf("File doesn't exist!\n");
		return;
	}
	if(access(filename,R_OK) != 0){
		printf("Insufficient permissions for reading file!\n");
		return;
	}
	
	loadFile(filename,ca_intern);
}

// Function to read a file located at 'filename' into the cell array 'ca_intern'
// Args: file_name, cell_array_to_load_into
void loadFile(char* filename,cellArray* ca_intern){
	printf("Loading state '%s' \n",filename);

	FILE *f = fopen(filename, "r"); // open file in read only mode

	if (f == NULL) {
		printf("Error opening file. Exit.\n");
		exit(EXIT_FAILURE);
	}

	fscanf(f,"%i\n",&ca_intern->width);
	fscanf(f,"%i\n",&ca_intern->height);

	printf("width = %i; height = %i \n",ca_intern->width,ca_intern->height);

	ca_intern->cells = create2d(ca_intern->width,ca_intern->height);

	for (int j = ca_intern->height-1; j >= 0; j--){
		char * row = calloc(ca_intern->width * sizeof(cell_t),sizeof(cell_t));
		fscanf(f,"%255[^\n]\n",row);
		for (int i = 0; i < ca_intern->width; i++){
			// ca_intern->cells[i][j] = row[i] - '0';
			ca_intern->cells[i][j] = C2I(row[i]);
		}
		free(row);
	}

	for (int j = 0; j < ca_intern->height; j++){
		for (int i = 0; i < ca_intern->width; i++){
			printf("%i",ca_intern->cells[i][j]);
		}
		printf("\n");
	}

	fclose(f);
	printf("Done loading state.\n");
}

// Function to generate file save dialog using Zenity, and then call saveFile()
void askSave(cellArray* ca_intern){
	char filename[STRLEN];

	FILE* fp;
	fp = popen("zenity --file-selection --save --confirm-overwrite --title='Select a File'","r");
	fgets(filename, sizeof(filename), fp);
	pclose(fp);
	for (int i = 0; i < STRLEN; i++){
		if(filename[i] == '\n'){
			filename[i] = '\0';
			break;
		}
	}
	printf("File: '%s'\n",filename);

	if (access(filename,F_OK) == 0){
		printf("File already exists! Not overwriting.\n");
		return;
	}
	
	saveFile(ca_intern,filename);
}

// Save status of _states to a file at location _filename
void saveFile(cellArray* states,char* filename){

	FILE* f = fopen(filename,"w");

	fprintf(f,"%i\n",states->width);
	fprintf(f,"%i\n",states->height);

	for (int j = states->height-1; j >= 0; j--){
		char * row = calloc(states->width * sizeof(cell_t),sizeof(cell_t));
		for (int i = 0; i < states->width; i++){
			// row[i] = states->cells[i][j] + '0';
			row[i] = I2C(states->cells[i][j]);
		} 
		fprintf(f,"%s\n",row);
		free(row);
	}

	fclose(f);
	printf("Done writing file.\n");

}