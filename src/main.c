/*
Final Project for the PHY2027 module
Author: Harrison Nicholls (hen204)
Date: 01/12/19
*/

/*
Program description: This program is a C implementation of Conway's Game of Life. Please see the Wikipedia
					 page for a description of its mechanics: https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life.
					 My implementation uses OpenGL to render squares to the screen, representing cells. Their colour
					 changes to indicate if they are alive or dead. Launching the binary will allow the user to choose
					 the dimensions of the array of cells, as well as the rules with which to simulate the game.
					 The number keys are used to select the pattern to be placed when the user left clicks on a cell; 
					 the current selection is indicated in the top right. You can also press Space to play/pause the game.
					 The up and down arrow keys change the simulation speed. 

					 Please see the GitLab repository for more details, including example screenshots, and system requirements:
					 https://gitlab.com/harrisonnicholls13/conways-game-of-life
					 
*/

#include <conway.h>

// Main function
int main(int argc, char *argv[]){ // Takes command line arguments
	printf("Welcome to Conway's Game of Life. Implementation by Harrison Nicholls. \n");

	int window_width, window_height;
	int req_width, req_height;

	bool play = false;
	bool loading = false;
	bool saving = false;

	float loop_time = 1;
	double space_cooldown = COOLDOWN;
	int placemode = 1; // numbers 1-5 choose what is placed on a left click
	bool rainbow = false;

	float color_matrix[COLORNUM][COLORLEN] = {
											 {0.6f,0.6f,0.6f}, // DEAD COLOUR
											 {0.9f,0.1f,0.1f}  // ALIVE COLOUR
											 };
	
	char survival_rules[STRLEN]; // They shouldn't need more than 8
	char birth_rules[STRLEN]; 

	bool ask_dims = true;
	bool ask_rules = true;

	// Command line arguments:
	// --dims  width height  ->  Allows the user to set the cell array dimensions and rules prior to launch
	// --rules survive born  ->  Allows the user to set the rules for survival and birth prior to launch
	// --play                -> Starts the game unpaused (playing)
	// --rainbow             -> Enable rainbow mode
	
	for (int i = 1; i < argc; i++){
		if (AVCHK("--dims")) {
			if (argc - i >= 3) { // Check that there are 2 numbers following the flag
				i += 1;
				req_width = atoi(argv[i]);
				i += 1;
				req_height = atoi(argv[i]);
				ask_dims = false;
			} else {
				printf("ERROR: Incorrect format for dimension flag (-d). \n");
			}
		} else if (AVCHK("--rules")) {
			if (argc - i >= 3) {
				i += 1;
				strcpy(survival_rules,argv[i]);
				i += 1;
				strcpy(birth_rules,argv[i]);
				ask_rules = false;
			} else {
				printf("ERROR: Incorrect format for rules flag (-r). \n");
			}
		} else if (AVCHK("--play")) {
			play = true;
		} else if (AVCHK("--rainbow")) {
			rainbow = true;
		}
		
	}

	if (ask_dims) {
		int good = 0; // Number of successfully read items from scanf

		while (good != 2) { // Needs to read 2 arguments
			req_width = 0;
			req_height = 0;
			if (good != 2) printf("Enter the dimensions of the grid of cells in the form 'width height'...\n>");
			good = scanf("%i %i",&req_width,&req_height); // Get user input and set 'good' to status
			// getchar(); // Skip extra char
			// Using fact that scanf() returns an integer equal to the number of successfully read values.
		}
	}

	// Checking for silly values of width and height.
	// Choosing a really big number (e.g. 800) will make the game very slow.
	if(req_width > MAX_CELLS_WIDE){
		printf("The requested width dimension of the array of cells is unreasonably large (w > %i). Setting to %i.\n",MAX_CELLS_WIDE,MAX_CELLS_WIDE);
		req_width = MAX_CELLS_WIDE;
	}
	if(req_height > MAX_CELLS_TALL){
		printf("The requested height dimension of the array of cells is unreasonably large (h > %i). Setting to %i.\n",MAX_CELLS_TALL,MAX_CELLS_TALL);
		req_height = MAX_CELLS_TALL;
	}

	if (req_width*CELL_SIZE > MAX_WINDOW_WIDTH) {
		printf("Requested combination of width and cell size results in a window that is too wide. Restricting to maximum allowed; cells outside the window will be updated but not rendered.\n");
		window_width = MAX_WINDOW_WIDTH;
	} else {
		window_width = (CELL_SIZE+SPACING) * req_width + SPACING;
	}

	if (req_height*CELL_SIZE > MAX_WINDOW_HEIGHT) {
		printf("Requested combination of height and cell size results in a window that is too tall. Restricting to maximum allowed; cells outside the window will be updated but not rendered.\n");
		window_height = MAX_WINDOW_HEIGHT;
	} else {
		window_height = (CELL_SIZE+SPACING) * req_height + SPACING;
	}

	if (ask_rules){
		// See this website for a nice set of possible rules
		// http://www.mirekw.com/ca/rullex_life.html
		// Also try:
		// 45 / 1256

		printf("Enter the number of neighbours that a cell must have for it to survive (Conway sets '32').\n>");
		scanf("%s",survival_rules);

		printf("Enter the number of neighbours that a cell must have for it to be born (Conway sets '3').\n>");
		scanf("%s",birth_rules);
	}

	// Just in case
	req_width = abs(req_width);
	req_height = abs(req_height);

	// --------------------------------------------------
	// DONE WITH USER SET UP ----------------------------
	// --------------------------------------------------

	cellArray* states = (cellArray*) malloc(sizeof(cellArray));
	states->width = req_width;
	states->height = req_height;
	states->cells = create2d(states->width+1,states->height+1);
	memcpy(states->color_matrix,color_matrix,sizeof(float) * COLORLEN * COLORNUM);

	cellArray* temp_states = (cellArray*) malloc(sizeof(cellArray));
	temp_states->width = req_width;
	temp_states->height = req_height;
	temp_states->cells = create2d(temp_states->width+1,temp_states->height+1);
	memcpy(temp_states->color_matrix,color_matrix,sizeof(float) * COLORLEN * COLORNUM);

	double xcursor, ycursor; // Could maybe be a float, but it doesn't really matter
	int m1_state, m2_state;
	
    if (!glfwInit()) { // Check if glfw works
		printf("Error accessing GLFW! \n");
    	exit(EXIT_FAILURE);
    }

	// create window
	glfwWindowHint(GLFW_RESIZABLE,GLFW_FALSE);
	glfwWindowHint(GLFW_FOCUS_ON_SHOW,GLFW_TRUE);
	glfwWindowHint(GLFW_DECORATED,GLFW_FALSE);
	glfwWindowHint(GLFW_FLOATING,GLFW_TRUE);
    GLFWwindow* window = glfwCreateWindow(window_width, window_height, "Conway's Game of Life", NULL, NULL);

	// If window creation fails
    if (window == NULL) {
		printf("Error creating window! \n");
    	glfwTerminate();
		exit(EXIT_FAILURE);
    }
	
	// More window setup
	glfwMakeContextCurrent(window);
    glfwSwapInterval(VSYNC);
	glfwGetFramebufferSize(window, &window_width, &window_height);
	glViewport(0, 0, window_width, window_height);
	glOrtho(0, window_width, 0, window_height, -1, 1); 	// Setup viewport with 0,0 in bottom left of the window

	// Set some random cells to alive so we have a starting condition
	set(2,6,states,ALIVE);
	set(3,6,states,ALIVE);
	set(2,7,states,ALIVE);
	set(3,7,states,ALIVE);
	set(17,12,states,ALIVE);
	set(18,11,states,ALIVE);
	set(17,13,states,ALIVE);
	set(16,12,states,ALIVE);
	set(23,24,states,ALIVE);

	// Loop timing variables
	double tDelta,tLast,tNow;
	double tFirst = glfwGetTime();
	tLast = 0;

	// BEGIN GAME LOOP
    while (!glfwWindowShouldClose(window)) {
		tNow = glfwGetTime() - tFirst;
		tDelta = tNow - tLast; // time since last game loop
		space_cooldown -= 0.01;

		// Get HID states
		glfwPollEvents();
		glfwGetCursorPos(window, &xcursor, &ycursor);
		m1_state = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT);
		m2_state = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_RIGHT);

		// make all cells alive
		if (KP(GLFW_KEY_A)) {
			set2d(states->width, states->height,states->cells, ALIVE);
		}

		// make all cells dead
		if (KP(GLFW_KEY_D)){
			set2d(temp_states->width, temp_states->height,states->cells, DEAD);
		}

		// randomise grid
		if(KP(GLFW_KEY_R)){
			for (int i = 0; i < states->width; i++){
				for(int j = 0; j < states->height; j++){
					set(i,j,states,getRand(2));
				}
			}
		}

		// speed up simulation
		if (KP(GLFW_KEY_UP)) {
			if (loop_time >= LOOP_TIME_LOW) loop_time -= 0.0005;
		}

		// slow down simulation
		if (KP(GLFW_KEY_DOWN)) {
			if (loop_time <= LOOP_TIME_HGH) loop_time += 0.0005;
		}

		// Use number keys to change what's placed on m1 down
		for (int i = GLFW_KEY_1; i <= GLFW_KEY_9;i++){
			if(KP(i)) placemode = (i - GLFW_KEY_1 + 1);
		}

		// Press Escape to exit program
		if (KP(GLFW_KEY_ESCAPE)) {
			glfwSetWindowShouldClose(window, GL_TRUE);
		}
		// Press Space to toggle play/pause of simulation
		if (KP(GLFW_KEY_SPACE)) {
			if (space_cooldown <= 0){
				play = !(play);
				space_cooldown = COOLDOWN;

				if(play){
					printf("Game is now playing. \n");
				} else {
					printf("Game is now paused. \n");
				}
			}
		}

		if(KP(GLFW_KEY_F9) && loading == false){
			if (USE_DIALOGS){
				loading = true;
				askLoad(states);
				loading = false;
			}
		}

		if(KP(GLFW_KEY_F5) && saving == false){
			if(USE_DIALOGS){
				saving = true;
				askSave(states);
				saving = false;
			}
		}

		if (m1_state == true) { // on m1 held
			place(clickedCX(xcursor), clickedCY(ycursor,window_height),placemode,states);
		} else if (m2_state == true) { // on m2 held
			set(clickedCX(xcursor), clickedCY(ycursor,window_height),states,DEAD);
		}

		// cycle colours if rainbow is enabled
		// different frequencies make it more interesting
		if (rainbow){
			states->color_matrix[ALIVE][0] = powf(sinf(tNow*0.7*RAINBOWSPEED),2);
			states->color_matrix[ALIVE][1] = powf(sinf(tNow*0.3*RAINBOWSPEED),2);
			states->color_matrix[ALIVE][2] = powf(sinf(tNow*0.5*RAINBOWSPEED),2);
		}

		// Perform render operations
		glfwSwapBuffers(window);
		glMatrixMode(GL_PROJECTION);
	    glLoadIdentity();
		glClear(GL_COLOR_BUFFER_BIT);
		render(states,play, placemode, window_width, window_height);

		// Conditionally perform update operations
		if (tDelta > loop_time && play == 1){
			update(temp_states, states, survival_rules, birth_rules);
			fflush(stdout); // In case I forget a linebreak
			tLast = tNow;
		}

    } // End of loop

	printf("Exiting...\n");
	destroy2d(states->width + 1,states->cells);
	destroy2d(temp_states->width + 1,temp_states->cells);
    glfwDestroyWindow(window);
    glfwTerminate();
	printf("Bye!\n");
	exit(EXIT_SUCCESS);
}
